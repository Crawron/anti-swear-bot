import { Adapter, CommandGroup } from "@enitoni/gears-discordjs"
import { Bot } from "@enitoni/gears"

import { SwearingService, SettingsService } from "./services"
import { swearCommand, prefixGroup } from "./commands"
import { matchHuman } from "./matchHuman"

import "dotenv/config"

const group = new CommandGroup({
	matcher: matchHuman(),
	commands: [prefixGroup, swearCommand]
})

const adapter = new Adapter({ token: process.env.TOKEN! })
const bot = new Bot({
	adapter,
	group,
	services: [SettingsService, SwearingService]
})

bot.start()
