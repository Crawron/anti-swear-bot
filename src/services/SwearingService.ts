import { Service } from "@enitoni/gears-discordjs"

import { readFileAsync } from "../helpers/readFileAsync"

interface BadWordsStore {
	[key: string]: string[]
}

export class SwearingService extends Service {
	badWords!: BadWordsStore
	regions: string[] = ["jp", "en", "es"]

	async serviceDidInitialize() {
		this.loadBadWords()
	}

	async loadBadWords() {
		for (const region of this.regions) {
			const fileStr = await readFileAsync(`badwordlists/${region}.txt`)
			const badWordList = fileStr.toString().split("\n")

			this.badWords = { ...this.badWords, [region]: [] }
			this.badWords[region] = badWordList
		}
	}

	async checkBadWord(message: string, region: string) {
		const trimmedMessage = message.replace(/ |\n/g, "").toLowerCase()

		for (const word of this.badWords[region]) {
			const badWordRegex = RegExp(`.*${word}.*`, "g")
			if (badWordRegex.test(trimmedMessage)) return true
		}

		return false
	}
}
