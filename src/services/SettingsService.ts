import * as knex from "knex"
import { Service } from "@enitoni/gears-discordjs"

export class SettingsService extends Service {
	private dbClient!: knex

	async serviceDidInitialize() {
		this.dbClient = knex({
			client: "sqlite3",
			useNullAsDefault: false,
			connection: {
				filename: "db.sqlite"
			}
		})

		const tableExists = await this.dbClient.schema.hasTable("Guilds")
		if (!tableExists) this.createDb()
	}

	async createDb() {
		this.dbClient.schema
			.createTable("Guilds", (table) => {
				table.string("guildId").primary()
				table.string("region").defaultTo("en")
				table.boolean("shouldDelete").defaultTo(false)
			})
			.then()
	}

	async addGuild(guildId: string) {
		this.dbClient("Guilds")
			.insert({ guildId })
			.then()
	}

	async deleteGuild(guildId: string) {
		this.dbClient("Guilds")
			.where({ guildId })
			.delete()
			.then()
	}

	async getGuildSettings(guildId: string): Promise<GuildSettings[]> {
		return this.dbClient("Guilds")
			.where({ guildId })
			.select("region", "shouldDelete")
			.then<GuildSettings[]>()
	}

	async setGuildRegion(guildId: string, region: string) {
		this.dbClient("Guilds")
			.update({ region })
			.where({ guildId })
			.then()
	}

	async setGuildDelete(guildId: string, shouldDelete: boolean) {
		this.dbClient("Guilds")
			.update({ shouldDelete })
			.where({ guildId })
			.then()
	}
}

interface GuildSettings {
	region: string
	shouldDelete: boolean
}
