import { CommandMatcher } from "@enitoni/gears-discordjs"

export const matchHuman: () => CommandMatcher<{}> = () => async (context) => {
	if (!context.message.author.bot) return context
}
