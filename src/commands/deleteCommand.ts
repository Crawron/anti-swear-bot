import { Command } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"
import { SettingsService } from "../services"
import { getGuildSettings, checkPermission } from "./middleware"

export const deleteCommand = new Command({
	matcher: matchPrefixes("delete"),
	middleware: [
		checkPermission("MANAGE_MESSAGES"),
		getGuildSettings,
		async (context) => {
			const { bot, message, state } = context
			const settingsService = bot.manager.getService(SettingsService)

			await settingsService.setGuildDelete(
				message.guild.id,
				!state.shouldDelete
			)

			return message.channel.send(
				`**Messages with bad words will ${
					!state.shouldDelete ? "" : "_not_ "
				}be deleted.**`
			)
		}
	]
})
