import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"

import { SettingsService, SwearingService } from "../services"
import { checkPermission } from "./middleware"

export const regionCommand = new Command({
	matcher: matchPrefixes("region", "reg"),
	middleware: [
		checkPermission("MANAGE_MESSAGES"),
		async (context) => {
			const { message, bot, content: region } = context

			const settingsService = bot.manager.getService(SettingsService)
			const { regions } = bot.manager.getService(SwearingService)

			if (!regions.includes(region))
				return message.channel.send(
					`**Invalid region, available: ${regions}**`
				)

			await settingsService.setGuildRegion(message.guild.id, region)
			return message.channel.send(`**Server region set to ${region}**`)
		}
	]
})
