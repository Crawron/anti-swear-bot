import { Message } from "discord.js"
import { Command } from "@enitoni/gears-discordjs"

import { getGuildSettings, checkBadWord } from "./middleware"
import { matchAlways } from "@enitoni/gears"

export const swearCommand = new Command({
	matcher: matchAlways(),
	middleware: [
		getGuildSettings,
		checkBadWord,
		async (context) => {
			const { message, state } = context

			if (state.shouldDelete) message.delete()
			else message.react("⚠")

			const response = (await message.channel.send(
				`**No swearing, ${context.message.author.username}**. :warning:`
			)) as Message
			setTimeout(() => response.delete(), 2000)
		}
	]
})
