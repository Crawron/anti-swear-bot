import { Middleware } from "@enitoni/gears-discordjs"
import { PermissionResolvable } from "discord.js"

export const checkPermission: (
	permission: PermissionResolvable
) => Middleware = (permission: PermissionResolvable) => (context, next) => {
	if (!context.message.member.hasPermission(permission)) return
	else next()
}
