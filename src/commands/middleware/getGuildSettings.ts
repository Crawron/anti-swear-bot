import { Middleware } from "@enitoni/gears-discordjs"
import { SettingsService } from "../../services"

export interface GuildSettingsContext {
	region: string
	shouldDelete: boolean
}

export const getGuildSettings: Middleware<GuildSettingsContext> = async (
	context,
	next
) => {
	const { bot, message } = context
	const settingsServ = bot.manager.getService(SettingsService)

	const guildSettings = (await settingsServ.getGuildSettings(
		message.guild.id
	))[0]

	context.state.region = guildSettings.region
	context.state.shouldDelete = guildSettings.shouldDelete

	next()
}
