import { Middleware } from "@enitoni/gears-discordjs"
import { SwearingService } from "../../services"
import { GuildSettingsContext } from "./getGuildSettings"

interface BadWordContext {
	hasBadWord: boolean
}

export const checkBadWord: Middleware<
	BadWordContext & GuildSettingsContext
> = async (context, next) => {
	const swearingServ = context.bot.manager.getService(SwearingService)
	const hasBadWord = await swearingServ.checkBadWord(
		context.content,
		context.state.region
	)

	if (!hasBadWord) return

	context.state.hasBadWord = true
	next()
}
