import { CommandGroup } from "@enitoni/gears-discordjs"
import { matchPrefixes } from "@enitoni/gears"

import { regionCommand } from "./regionCommand"
import { deleteCommand } from "./deleteCommand"

export const prefixGroup = new CommandGroup({
	matcher: matchPrefixes("."),
	commands: [regionCommand, deleteCommand]
})
