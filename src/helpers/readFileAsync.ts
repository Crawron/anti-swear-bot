import { PathLike, readFile } from "fs"

export async function readFileAsync(
	path: PathLike | number,
	encoding?: string
): Promise<string | Buffer> {
	return new Promise((resolve, reject) => {
		readFile(path, encoding, (err, data) => {
			if (err) reject(err)
			else resolve(data)
		})
	})
}
